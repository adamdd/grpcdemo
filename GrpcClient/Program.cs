﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using GrpcServer.Protos;

var channel = GrpcChannel.ForAddress("http://localhost:5000");

var greeterClient = new Greeter.GreeterClient(channel);
var hapticClient = new Haptic.HapticClient(channel);
var movementClient = new NodeMovement.NodeMovementClient(channel);
var ledClient = new EmitLED.EmitLEDClient(channel);
var bidirectionalClient = new BidirectionStream.BidirectionStreamClient(channel);

var tokenSource = new CancellationTokenSource();
CancellationToken ct = tokenSource.Token;

var tokenSource2 = new CancellationTokenSource();
CancellationToken ct2 = tokenSource2.Token;

var greetingReply = await greeterClient.SayHelloAsync(new HelloRequest { Name = "Deep Dive" });
Console.WriteLine(greetingReply.Message);
Console.WriteLine("-----------------------------------------------------\n");

Console.WriteLine("Unary RPC");
var vibrateReply = await hapticClient.VibrateSingleNodeAsync(
    new VibrateRequest
    {
        Bodypart = "left_shoulder",
        Intensity = 5,
        Duration = 3,
        Mode = "pulse"
    }
);
Console.WriteLine("Reply from server: " + vibrateReply.Message);
Console.WriteLine("-----------------------------------------------------\n");

Console.ReadLine();

Console.WriteLine("Server streaming RPC");

using (var call = movementClient.Track(new Empty { }, cancellationToken: ct))
{
    // Read response in background task.
    var responseTask = Task.Run(async () =>
    {
        await foreach (var current in call.ResponseStream.ReadAllAsync())
        {
            Console.WriteLine($"Node: {current.Bodypart} X: {current.X} Y: {current.Y} Z: {current.Z}");
        }
    });

    // Background task to kill the above task in 5 seconds
    var killingTask = Task.Run(async () =>
    {
        await Task.Delay(TimeSpan.FromSeconds(3));
        tokenSource.Cancel();
    });
    try
    {
        await responseTask;
    }
    catch (RpcException err)
    {
        Console.WriteLine(err.Status);
    }
    await killingTask;
}

Console.WriteLine("-----------------------------------------------------\n");

Console.ReadLine();

Console.WriteLine("Client streaming RPC");
using (var call = ledClient.StreamLEDFromClient())
{
    Random random = new Random();
    for (var i = 0; i < 20; i++)
    {
        var randomBodypart = new string[] { "left_shoulder", "left_elbow", "left_wrist", "head" }[random.Next(0, 4)];
        var randomMode = new string[] { "blink", "continuous" }[random.Next(0, 2)];
        var randomeStatus = new bool[] { true, false }[random.Next(0, 2)];
        var randomDuration = i;

        var node = new NodeRequest { Bodypart = randomBodypart, Mode = randomMode, Status = randomeStatus, Duration = randomDuration };
        await Task.Delay(TimeSpan.FromSeconds(1));
        await call.RequestStream.WriteAsync(node);
    }
    await call.RequestStream.CompleteAsync();

    LEDResponse endMessage = await call.ResponseAsync;
    Console.WriteLine($"From server: {endMessage.Message}");
}
Console.WriteLine("-----------------------------------------------------\n");

Console.ReadLine();

Console.WriteLine("Bidirectional Streaming (One to one)");
using (var call = bidirectionalClient.OneToOne())
{
    Int32 i = 0;
    while (true)
    {
        await Task.Delay(TimeSpan.FromSeconds(1));
        await call.RequestStream.WriteAsync(new BiRequest { RequestNumber = i + 1});
        await call.ResponseStream.MoveNext(ct2);
        var current = call.ResponseStream.Current;
        Console.WriteLine($"Received from server: {current.ResponseNumber}");
        if (current.ResponseNumber > 20) break;
        else i = current.ResponseNumber;
    }
    await call.RequestStream.CompleteAsync();
}
Console.WriteLine("-----------------------------------------------------\n");

Console.ReadLine();

Console.WriteLine("Bidirectional Streaming (Simultaneous)");
using (var call = bidirectionalClient.Simultaneous())
{
    var responseReaderTask = Task.Run(async () =>
    {
        while (await call.ResponseStream.MoveNext(ct2))
        {
            var current = call.ResponseStream.Current;
            Console.WriteLine("Incoming -> From server: " + current.ResponseNumber);
        }
    });

    for (var i = 1; i <= 10; i++)
    {
        BiRequest request = new BiRequest { RequestNumber = i };
        await Task.Delay(TimeSpan.FromSeconds(2));
        await call.RequestStream.WriteAsync(request);
    }
    await call.RequestStream.CompleteAsync();
    await responseReaderTask;
}