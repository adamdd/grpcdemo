﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcServer.Protos;

namespace GrpcServer.Services;

public class MovementService : NodeMovement.NodeMovementBase
{
    private readonly ILogger<MovementService> _logger;
    public MovementService(ILogger<MovementService> logger)
    {
        _logger = logger;
    }

    public override async Task Track(Empty request, IServerStreamWriter<MovementResponse> responseStream, ServerCallContext context)
    {
        Random random = new Random();
        int i = 1;
        try
        {
            while (!context.CancellationToken.IsCancellationRequested)
            {
                var randomBodypart = new string[] { "left_shoulder", "left_elbow", "left_wrist", "head" }[random.Next(0, 4)];
                var randomX = i;
                var randomY = i;
                var randomZ = i;

                var response = new MovementResponse { Bodypart = randomBodypart, X = randomX, Y = randomY, Z = randomZ };

                i++;
                // await Task.Delay(TimeSpan.FromSeconds(1));
                await responseStream.WriteAsync(response);
            }
        }
        catch (TaskCanceledException err)
        {
            Console.WriteLine(err.Message);
        }
    }
}

