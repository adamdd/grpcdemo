﻿using GrpcServer.Protos;
using Grpc.Core;

namespace GrpcServer.Services;

public class HapticService : Haptic.HapticBase
{
    private readonly ILogger<HapticService> _logger;
    public HapticService(ILogger<HapticService> logger)
    {
        _logger = logger;
    }

    public override Task<VibrateResponse> VibrateSingleNode(VibrateRequest request, ServerCallContext context)
    {
        return Task.FromResult(new VibrateResponse
        {
            Message = $"Will now vibrate {request.Bodypart} on level {request.Intensity} for {request.Duration}s in {request.Mode} mode."
        });
    }
}