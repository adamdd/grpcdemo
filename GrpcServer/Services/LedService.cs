﻿using System;
using Grpc.Core;
using GrpcServer.Protos;

namespace GrpcServer.Services;

public class LedService : EmitLED.EmitLEDBase
{
    private readonly ILogger<LedService> _logger;
    public LedService(ILogger<LedService> logger)
    {
        _logger = logger;
    }

    public override async Task<LEDResponse> StreamLEDFromClient(IAsyncStreamReader<NodeRequest> requestStream, ServerCallContext context)
    {
        Console.WriteLine("<-- Data stream from client start -->");
        await foreach (var message in requestStream.ReadAllAsync())
        {
            Console.WriteLine($"Incoming -> Node: {message.Bodypart} Mode: {message.Mode} Light: {message.Status} Duration: {message.Duration}s");
        }
        Console.WriteLine("<-- Data stream from client end -->");
        return new LEDResponse { Message = "All request received. Check service terminal for details" };
    }
}


