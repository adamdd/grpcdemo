using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcServer.Protos;

namespace GrpcServer.Services;

public class BidirectionService : BidirectionStream.BidirectionStreamBase
{
    private readonly ILogger<BidirectionService> _logger;
    public BidirectionService(ILogger<BidirectionService> logger)
    {
        _logger = logger;
    }

    public override async Task OneToOne(IAsyncStreamReader<BiRequest> requestStream, IServerStreamWriter<BiResponse> responseStream, ServerCallContext context)
    {
        Console.WriteLine("<-- Data stream from bidirectional client start -->");
        await foreach (var message in requestStream.ReadAllAsync())
        {
            Console.WriteLine($"Received from client: {message.RequestNumber}");
            await Task.Delay(TimeSpan.FromSeconds(1));
            await responseStream.WriteAsync(new BiResponse { ResponseNumber = message.RequestNumber + 1 });
        }
        Console.WriteLine("<-- Data stream from bidirectional client end -->");
    }

    public override async Task Simultaneous(IAsyncStreamReader<BiRequest> requestStream, IServerStreamWriter<BiResponse> responseStream, ServerCallContext context)
    {
        var responseNum = 1;
        // Read requests in a background task. Reading from client is twice as slow as sending to client
        var readTask = Task.Run(async () =>
        {
            Console.WriteLine("<-- Data stream from client start -->");
            await foreach (var message in requestStream.ReadAllAsync())
            {
                Console.WriteLine($"Incoming -> From request: {message.RequestNumber}");
            }
            Console.WriteLine("<-- Data stream from client end -->");
        });
        
        // Send responses until the client signals that it is complete.
        while (!readTask.IsCompleted)
        {
            await Task.Delay(TimeSpan.FromSeconds(1), context.CancellationToken);
            await responseStream.WriteAsync(new BiResponse { ResponseNumber = responseNum});
            responseNum++;
        }
    }
}
